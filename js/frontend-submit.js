/**
 * Adds front-end functionality to the form.
 */

// Image upload handler
function hsImageUpload(id) {

    document.getElementById(id + '-spinner').style.display = 'block';

    var fileData = jQuery('#' + id)[0].files[0];
    console.log(fileData);
    
    var formData = new FormData();
    formData.append('action', 'hs_img_upload');
    formData.append('file', fileData);

    var ajaxurl = '../wp-admin/admin-ajax.php';
    jQuery.ajax({
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        url: ajaxurl,

        success: function (response) {
            console.log(response);
            document.getElementById(id + '-spinner').style.display = 'none';
            document.getElementById(id + '-preview').src = response.url;
            document.getElementById(id + '-preview').style.display = 'block';
            document.getElementById(id + '-path').value = response.url;
        },

        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
}

// Handle package selection.
function selectPackage(id) {
    var basic = document.getElementById('basic-package');
	var enhanced = document.getElementById('enhanced-package');
	var type = document.getElementById('submission-type');
    
    if (id === 'basic-package') {
        basic.classList.add('package-selected');
		enhanced.classList.remove('package-selected');
		type.value = 'Sport Horse Basic';
	}
	
	if (id === 'enhanced-package') {
        enhanced.classList.add('package-selected');
		basic.classList.remove('package-selected');
		type.value = 'Sport Horse Enhanced';
    }
}

function hsShowPropType() {
    var propCategory = document.getElementById('property-category').value;

    if (propCategory === 'Agricultural Land') {
        document.getElementById('type-agri').style.display = 'block';
        document.getElementById('type-equine').style.display = 'none';
        document.getElementById('option-equine').value = '';
    }

    if (propCategory === 'Equine Stud') {
        document.getElementById('type-equine').style.display = 'block';
        document.getElementById('type-agri').style.display = 'none';
        document.getElementById('option-agri').value = '';
    }
}

function hsShowLoader() {
    var form = document.getElementById('frontend-submit');
    if (form.checkValidity()) {
        document.getElementById('form-spinner').style.display = 'inline-block';
    }    
}
