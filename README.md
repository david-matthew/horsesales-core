# README #

How to use the Horsesales Submit plugin.

### Instructions ###

* Place the shortcode `[hs_sport_horse_form]` on the page you want the sport horse form to appear.
* Place the shortcode `[hs_th_listing_form]` on the page you want the thoroughbred listing form to appear.
* Place the shortcode `[hs_th_auction_form]` on the page you want the thoroughbred auction form to appear.
* Place the shortcode `[hs_property_form]` on the page you want the property form to appear.
* Place the shortcode `[hs_marketplace_form]` on the page you want the marketplace form to appear.
* The above shortcodes all have two parameters: 
    * `url_submission`, which is the url of the page that should process the form (see below)
	* `url_tscs`, which is the link to the relevant terms and conditions page
* On the page where you want to process any of the horse-related forms (and redirect to the appropriate payment url), place the shortcode `[horse_submission_handler]`
* This shortcode has four parameters: 
    * `url_pay_spb`, which is the page for processing sport horse (basic) payments
	* `url_pay_spe`, which is the page for processing sport horse (enhanced) payments
    * `url_pay_thl`, which is the page for processing thoroughbred listing payments
    * `url_pay_tha`, which is the page for processing thoroughbred auction payments
* On the page where you want to process property submissions, place the shortcode `[prop_submission_handler]`
	* This shortcode has a single parameter: `url_pay_prop_listing`, which is the page for processing the property listing fee
* On the page where you want to process marketplace submissions, place the shortcode `[mkt_submission_handler]`
	* This shortcode has a single parameter: `url_pay_mkt_listing`, which is the page for processing the marketplace listing fee

### Contribution guidelines ###

Please ensure your code conforms to the [WordPress Coding Standards](https://github.com/WordPress/WordPress-Coding-Standards) before committing to this repository.