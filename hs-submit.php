<?php
/**
 * The main plugin file.
 *
 * @package      HorsesalesSubmit
 * Plugin Name:  Horsesales Submit
 * Description:  Embed various custom submission forms using shortcodes. See the README for instructions.
 * Version:      2.3.0
 * Author:       Horsesales Team
 * Author URI:   https://horsesales.ie
 * License:      GPL-3
 * License URI:  https://www.gnu.org/licenses/gpl-3.0.txt
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Current plugin version, manually defined for script version handling.
define( 'HS_SUBMIT_VERSION', '2.3.0' );

// Load the required files.
require plugin_dir_path( __FILE__ ) . 'forms/sport-horse.php';
require plugin_dir_path( __FILE__ ) . 'forms/thoroughbred-listing.php';
require plugin_dir_path( __FILE__ ) . 'forms/thoroughbred-auction.php';
require plugin_dir_path( __FILE__ ) . 'forms/horse-form-handler.php';
require plugin_dir_path( __FILE__ ) . 'forms/property.php';
require plugin_dir_path( __FILE__ ) . 'forms/property-form-handler.php';
require plugin_dir_path( __FILE__ ) . 'forms/marketplace.php';
require plugin_dir_path( __FILE__ ) . 'forms/marketplace-form-handler.php';
require plugin_dir_path( __FILE__ ) . 'inc/send-email.php';
require plugin_dir_path( __FILE__ ) . 'inc/upload.php';

/**
 * Register the script.
 */
function hs_register_script() {
	wp_register_script( 'frontend-submit', plugin_dir_url( __FILE__ ) . 'js/frontend-submit.js', array( 'jquery' ), HS_SUBMIT_VERSION, true );
}
add_action( 'init', 'hs_register_script' );

/**
 * Conditionally enqueue the script.
 */
function hs_print_script() {
	global $hs_should_add_script;

	if ( ! $hs_should_add_script ) {
		return;
	}

	wp_print_scripts( 'frontend-submit' );
}
add_action( 'wp_footer', 'hs_print_script' );
