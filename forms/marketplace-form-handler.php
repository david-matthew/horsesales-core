<?php
/**
 * The form submission handler for the marketplace.
 *
 * @package HorsesalesSubmit
 */

require_once ABSPATH . 'wp-admin/includes/media.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/image.php';

/**
 * Register the shortcode.
 */
add_shortcode( 'mkt_submission_handler', 'mkt_submission_handler_callback' );

/**
 * The shortcode callback function.
 *
 * @param array $attributes The shortcode attributes.
 */
function mkt_submission_handler_callback( $attributes ) {
	$atts = shortcode_atts(
		array(
			'url_pay_mkt_listing' => site_url() . '/set-the-marketplace-listing-payment-url',
		),
		$attributes,
		'mkt_submission_handler'
	);

	$is_valid_nonce = ( isset( $_POST['marketplace_form'] ) && wp_verify_nonce( $_POST['marketplace_form'] ) ) ? true : false;

	if ( ! $is_valid_nonce ) {
		?>

		<div class="alert alert-danger" role="alert">Invalid form submission.</div>

		<?php

	} else {
		$submission  = isset( $_POST['submission_type'] ) ? sanitize_text_field( wp_unslash( $_POST['submission_type'] ) ) : '';
		$first_name  = isset( $_POST['first_name'] ) ? sanitize_text_field( wp_unslash( $_POST['first_name'] ) ) : '';
		$last_name   = isset( $_POST['last_name'] ) ? sanitize_text_field( wp_unslash( $_POST['last_name'] ) ) : '';
		$contact_no  = isset( $_POST['contact_no'] ) ? sanitize_text_field( wp_unslash( $_POST['contact_no'] ) ) : '';
		$email       = isset( $_POST['user_email'] ) ? sanitize_email( wp_unslash( $_POST['user_email'] ) ) : '';
		$bus_name    = isset( $_POST['business_name'] ) ? sanitize_text_field( wp_unslash( $_POST['business_name'] ) ) : '';
		$bus_contact = isset( $_POST['business_contact'] ) ? sanitize_text_field( wp_unslash( $_POST['business_contact'] ) ) : '';
		$bus_email   = isset( $_POST['business_email'] ) ? sanitize_email( wp_unslash( $_POST['business_email'] ) ) : '';
		$address     = isset( $_POST['street_address'] ) ? sanitize_text_field( wp_unslash( $_POST['street_address'] ) ) : '';
		$county      = isset( $_POST['county'] ) ? sanitize_text_field( wp_unslash( $_POST['county'] ) ) : '';
		$bus_cat     = isset( $_POST['service_category'] ) ? $_POST['service_category'] : '';
		$description = isset( $_POST['service_desc'] ) ? sanitize_textarea_field( wp_unslash( $_POST['service_desc'] ) ) : '';
		$website     = isset( $_POST['website'] ) ? esc_url( $_POST['website'] ) : '';
		$google_maps = isset( $_POST['google_maps'] ) ? esc_url( $_POST['google_maps'] ) : '';
		$facebook    = isset( $_POST['facebook'] ) ? esc_url( $_POST['facebook'] ) : '';
		$instagram   = isset( $_POST['instagram'] ) ? esc_url( $_POST['instagram'] ) : '';
		$linkedin    = isset( $_POST['linkedin'] ) ? esc_url( $_POST['linkedin'] ) : '';
		$twitter     = isset( $_POST['twitter'] ) ? esc_url( $_POST['twitter'] ) : '';
		$feat_img    = isset( $_POST['feat_img_path'] ) ? esc_url( $_POST['feat_img_path'] ) : '';
		$bus_logo    = isset( $_POST['business_logo_path'] ) ? esc_url( $_POST['business_logo_path'] ) : '';
		$img_3       = isset( $_POST['img_3_path'] ) ? esc_url( $_POST['img_3_path'] ) : '';
		$img_4       = isset( $_POST['img_4_path'] ) ? esc_url( $_POST['img_4_path'] ) : '';
		$img_5       = isset( $_POST['img_5_path'] ) ? esc_url( $_POST['img_5_path'] ) : '';
		$youtube_url = isset( $_POST['video'] ) ? esc_url( $_POST['video'] ) : '';
		$redirect    = $atts['url_pay_mkt_listing'];

		$my_post = array(
			'post_title'   => $bus_name,
			'post_type'    => 'service',
			'post_content' => $description,
			'post_excerpt' => $description,
		);

		/**
		 * Everything after wp_insert_post happens after the
		 * post is created (as the created id is required).
		 */
		$created_post_id = wp_insert_post( $my_post );

		if ( ! empty( $bus_contact ) ) {
			update_post_meta( $created_post_id, 'service_phn', $bus_contact );
		} else {
			update_post_meta( $created_post_id, 'service_phn', $contact_no );
		}

		if ( ! empty( $bus_email ) ) {
			update_post_meta( $created_post_id, 'service_email', $bus_email );
		} else {
			update_post_meta( $created_post_id, 'service_email', $email );
		}

		if ( ! empty( $website ) ) {
			update_post_meta( $created_post_id, 'service_website', $website );
		}

		if ( ! empty( $address ) ) {
			update_post_meta( $created_post_id, 'service_address', $address );
		}

		if ( ! empty( $google_maps ) ) {
			update_post_meta( $created_post_id, 'service_gmaps', $google_maps );
		}

		if ( ! empty( $facebook ) ) {
			update_post_meta( $created_post_id, 'service_facebook', $facebook );
		}

		if ( ! empty( $instagram ) ) {
			update_post_meta( $created_post_id, 'service_instagram', $instagram );
		}

		if ( ! empty( $linkedin ) ) {
			update_post_meta( $created_post_id, 'service_linkedin', $linkedin );
		}

		if ( ! empty( $twitter ) ) {
			update_post_meta( $created_post_id, 'service_twitter', $twitter );
		}

		if ( ! empty( $county ) ) {
			wp_set_object_terms( $created_post_id, $county, 'service-location' );
		}

		if ( ! empty( $bus_cat ) ) {
			foreach ( $bus_cat as $bc ) {
				wp_set_object_terms( $created_post_id, sanitize_text_field( wp_unslash( $bc ) ), 'service-category', true );
			}
		}

		if ( ! empty( $feat_img ) ) {
			$feat_img_id = media_sideload_image( $feat_img, $created_post_id, $bus_name, 'id' );
			update_post_meta( $created_post_id, '_thumbnail_id', $feat_img_id );
		}

		if ( ! empty( $bus_logo ) ) {
			media_sideload_image( $bus_logo, $created_post_id, $bus_name, 'id' );
			update_post_meta( $created_post_id, 'service_logo_url', $bus_logo );
		}

		if ( ! empty( $img_3 ) ) {
			media_sideload_image( $img_3, $created_post_id, $bus_name, 'id' );
			update_post_meta( $created_post_id, 'service_additional_img_1', $img_3 );
		}

		if ( ! empty( $img_4 ) ) {
			media_sideload_image( $img_4, $created_post_id, $bus_name, 'id' );
			update_post_meta( $created_post_id, 'service_additional_img_2', $img_4 );
		}

		if ( ! empty( $img_5 ) ) {
			media_sideload_image( $img_5, $created_post_id, $bus_name, 'id' );
			update_post_meta( $created_post_id, 'service_additional_img_3', $img_5 );
		}

		if ( ! empty( $youtube_url ) ) {
			$regex = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
			preg_match( $regex, $youtube_url, $match );
			$youtube_id = $match[1];
			update_post_meta( $created_post_id, 'service_video_id', $youtube_id );
		}

		hs_send_admin_notification( $first_name . ' ' . $last_name, $contact_no, $email, $bus_name, $submission );

		?>

		<div class="alert alert-success" role="alert">Thank you for your submission!</div>
		<a class="btn btn-submit" href="<?php echo esc_url( $redirect ); ?>">Confirm & Pay</a>

		<?php
	}
}
