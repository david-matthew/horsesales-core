<?php
/**
 * The form allowing users to submit a thoroughbred listing.
 *
 * @package HorsesalesSubmit
 */

/**
 * Register the shortcode.
 */
add_shortcode( 'hs_th_listing_form', 'hs_th_listing_form_callback' );

/**
 * The shortcode callback function.
 *
 * @param array $attributes The shortcode attributes.
 */
function hs_th_listing_form_callback( $attributes ) {
	global $hs_should_add_script;
	$hs_should_add_script = true;

	$atts = shortcode_atts(
		array(
			'url_submission' => site_url() . '/set-the-form-submission-handler-url',
			'url_tscs'       => site_url() . '/set-the-tscs-url',
		),
		$attributes,
		'hs_th_listing_form'
	);

	?>

	<div class="alert alert-info alert-dismissible fade show" role="alert">

		<span>Note that fields marked with an asterisk <span class="required">*</span> are required.</span>

		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

	</div>

	<form id="frontend-submit" action="<?php echo esc_url( $atts['url_submission'] ); ?>" method="post">

		<?php wp_nonce_field( -1, 'add_horse_form' ); ?>

		<h3	data-toggle="collapse" data-target="#user-details"><i class="fa fa-user-circle" aria-hidden="true"></i>Your Details</h3>
		<hr>

		<div id="user-details" class="collapse show form-section" aria-expanded="true">

			<div class="form-row">

				<div class="col-6 form-group">

					<label for="first-name">First Name<span class="required">*</span></label>

					<input required type="text" id="first-name" name="first_name" class="form-control">

				</div>

				<div class="col-6 form-group">

					<label for="last-name">Last Name<span class="required">*</span></label>

					<input required type="text" id="last-name" name="last_name" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="contact-no">Contact Number<span class="required">*</span></label>

					<input required type="tel" id="contact-no" name="contact_no" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="user-email">Email Address<span class="required">*</span></label>

					<input required type="email" id="user-email" name="user_email" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

				<a class="btn btn-next" data-toggle="collapse" href="#horse-details">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>			

		</div>

		<h3 data-toggle="collapse" data-target="#horse-details"><i class="fa fa-list-alt" aria-hidden="true"></i>Horse Details</h3>
		<hr>

		<div id="horse-details" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="listing-title">Listing Title<span class="required">*</span></label>

					<input required type="text" id="listing-title" name="listing_title" class="form-control" placeholder="Try to keep this to four/five words, or less if possible.">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="horse-desc">Description<span class="required">*</span></label>

					<textarea required id="horse-desc" name="horse_desc" cols="40" rows="6" class="form-control" placeholder="Enter a description of your horse here. Stick to the highlights, and think of points that would attract potential buyers."></textarea>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="horse-vices">Vices</label>

					<textarea id="horse-vices" name="horse_vices" cols="40" rows="2" class="form-control" placeholder="List the vices here (if any)."></textarea>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-price">Price (€)</label>

					<input type="number" id="horse-price" name="horse_price" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-name">Horse Name</label>

					<input type="text" id="horse-name" name="horse_name" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-sire">Sire</label>

					<input type="text" id="horse-sire" name="horse_sire" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-dam">Dam</label>

					<input type="text" id="horse-dam" name="horse_dam" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-year">Year of Birth</label>

					<input type="text" id="horse-year" name="horse_year" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-colour">Colour</label>

					<input type="text" id="horse-colour" name="horse_colour" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-height">Height</label>

					<input type="text" id="horse-height" name="horse_height" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-gender">Gender<span class="required">*</span></label>

					<select required name="horse_gender" id="horse-gender" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<option value="Colt">Colt</option>
						<option value="Filly">Filly</option>
						<option value="Gelding">Gelding</option>
						<option value="Mare">Mare</option>
						<option value="Stallion">Stallion</option>
					</select>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="race-type">Race Type<span class="required">*</span></label>

					<select required name="race_type" id="race-type" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<option value="National Hunt">National Hunt</option>
						<option value="Flat">Flat</option>
						<option value="Dual Purpose">Dual Purpose</option>
						<option value="Unraced">Unraced</option>
					</select>

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="th-horse-category">Category<span class="required">*</span></label>

					<select required name="th_horse_category" id="th-horse-category" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<option value="Broodmare">Broodmare</option>
						<option value="Foal">Foal</option>
						<option value="Horse in Training">Horse in Training</option>
						<option value="Point to Pointer">Point to Pointer</option>
						<option value="Store Horse">Store Horse</option>
						<option value="Yearling">Yearling</option>
					</select>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-location">Current Location<span class="required">*</span></label>

					<input required type="text" id="horse-location" name="horse_location" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-origin">Country of Origin</label>

					<input type="text" id="horse-origin" name="horse_origin" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#media">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#media"><i class="fa fa-picture-o" aria-hidden="true"></i>Images / Video</h3><hr>

		<div id="media" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Featured image<span class="required">*</span></label><br>

					<div id="hs-featured-img-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-featured-img-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input required type="file" name="hs_featured_img" id="hs-featured-img" value="" accept="image/*" onchange="hsImageUpload('hs-featured-img'); return false;">
					<input type="text" name="hs_featured_img_path" id="hs-featured-img-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 2</label><br>

					<div id="hs-img-2-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-2-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_2" id="hs-img-2" value="" accept="image/*" onchange="hsImageUpload('hs-img-2'); return false;"/>
					<input type="text" name="hs_img_2_path" id="hs-img-2-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 3</label><br>

					<div id="hs-img-3-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-3-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_3" id="hs-img-3" value="" accept="image/*" onchange="hsImageUpload('hs-img-3'); return false;"/>
					<input type="text" name="hs_img_3_path" id="hs-img-3-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 4</label><br>

					<div id="hs-img-4-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-4-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_4" id="hs-img-4" value="" accept="image/*" onchange="hsImageUpload('hs-img-4'); return false;"/>
					<input type="text" name="hs_img_4_path" id="hs-img-4-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 5</label><br>

					<div id="hs-img-5-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-5-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_5" id="hs-img-5" value="" accept="image/*" onchange="hsImageUpload('hs-img-5'); return false;"/>
					<input type="text" name="hs_img_5_path" id="hs-img-5-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="horse-video">YouTube/Vimeo URL</label>

					<input type="url" id="horse-video" name="horse_video" class="form-control" placeholder="A valid URL must start with http or https">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#docs">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#docs"><i class="fa fa-files-o" aria-hidden="true"></i>Pedigree / Certs etc</h3><hr>

		<div id="docs" class="collapse form-section">

			<div class="form-row">

				<div class="col-12">

					<div class="alert alert-info alert-dismissible fade show" role="alert">

						<span>Images only. Please screenshot any pdfs/word docs.</span>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					</div>

				</div>

				<div class="col-12 form-group">

					<label>Pedigree<span class="required">*</span></label><br>

					<div id="hs-pedigree-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-pedigree-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input required type="file" name="hs_pedigree" id="hs-pedigree" value="" accept="image/*" onchange="hsImageUpload('hs-pedigree'); return false;"/>
					<input type="text" name="hs_pedigree_path" id="hs-pedigree-path" value="" hidden/>

				</div>

				<div class="col-12 form-group">

					<label>Horse Passport (First Page)</label><br>

					<div id="hs-passport-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-passport-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_passport" id="hs-passport" value="" accept="image/*" onchange="hsImageUpload('hs-passport'); return false;"/>
					<input type="text" name="hs_passport_path" id="hs-passport-path" value="" hidden/>

				</div>

				<div class="col-12 form-group">

					<label>Reproduction Status Certificate (Mares Only)</label><br>

					<div id="hs-reprod-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-reprod-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_reprod" id="hs-reprod" value="" accept="image/*" onchange="hsImageUpload('hs-reprod'); return false;"/>
					<input type="text" name="hs_reprod_path" id="hs-reprod-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#confirmation">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#confirmation"><i class="fa fa-check-square-o" aria-hidden="true"></i>Confirmation</h3><hr>

		<div id="confirmation" class="collapse form-section">

			<div class="form-row">

				<div class="col-12">

					<input type="text" id="submission-type" name="submission_type" value="Thoroughbred Listing" hidden required />

					<label>Terms and Conditions<span class="required">*</span></label><br>

					<input required type="checkbox" id="tcs" name="tcs" value="yes" /> By ticking this box you accept our <a href="<?php echo esc_url( $atts['url_tscs'] ); ?>" target="_blank">Terms and Conditions</a>.<br/>

					<input required type="checkbox" id="encumbrance" name="encumbrance" value="yes" /> By ticking this box you declare that you know of no encumbrance to sell this horse. See <a href="<?php echo esc_url( $atts['url_tscs'] ); ?>" target="_blank">here</a> for further details.<br/>

					<button class="btn btn-submit mt-3" type="submit" onclick="hsShowLoader()">Submit & Pay</button>

					<div id="form-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>

				</div>

			</div>

		</div>

	</form>

	<?php
}

