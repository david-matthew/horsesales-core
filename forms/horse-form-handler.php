<?php
/**
 * The form submission handler for horses (sport horses, thoroughbred listings and auctions).
 *
 * @package HorsesalesSubmit
 */

require_once ABSPATH . 'wp-admin/includes/media.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/image.php';

/**
 * Register the shortcode.
 */
add_shortcode( 'horse_submission_handler', 'horse_submission_handler_callback' );

/**
 * The shortcode callback function.
 *
 * @param array $attributes The shortcode attributes.
 */
function horse_submission_handler_callback( $attributes ) {
	$atts = shortcode_atts(
		array(
			'url_pay_spb' => site_url() . '/set-the-sport-horse-basic-payment-url',
			'url_pay_spe' => site_url() . '/set-the-sport-horse-enhanced-payment-url',
			'url_pay_thl' => site_url() . '/set-the-thoroughbred-listing-payment-url',
			'url_pay_tha' => site_url() . '/set-the-thoroughbred-auction-payment-url',
		),
		$attributes,
		'horse_submission_handler'
	);

	$is_valid_nonce = ( isset( $_POST['add_horse_form'] ) && wp_verify_nonce( $_POST['add_horse_form'] ) ) ? true : false;

	if ( ! $is_valid_nonce ) {
		?>

		<div class="alert alert-danger" role="alert">Invalid form submission.</div>

		<?php

	} else {

		$submission_type = isset( $_POST['submission_type'] ) ? sanitize_text_field( wp_unslash( $_POST['submission_type'] ) ) : '';
		$first_name      = isset( $_POST['first_name'] ) ? sanitize_text_field( wp_unslash( $_POST['first_name'] ) ) : '';
		$last_name       = isset( $_POST['last_name'] ) ? sanitize_text_field( wp_unslash( $_POST['last_name'] ) ) : '';
		$contact_no      = isset( $_POST['contact_no'] ) ? sanitize_text_field( wp_unslash( $_POST['contact_no'] ) ) : '';
		$email           = isset( $_POST['user_email'] ) ? sanitize_email( wp_unslash( $_POST['user_email'] ) ) : '';
		$listing_title   = isset( $_POST['listing_title'] ) ? sanitize_text_field( wp_unslash( $_POST['listing_title'] ) ) : '';
		$description     = isset( $_POST['horse_desc'] ) ? sanitize_textarea_field( wp_unslash( $_POST['horse_desc'] ) ) : '';
		$horse_gender    = isset( $_POST['horse_gender'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_gender'] ) ) : '';
		$sp_categories   = isset( $_POST['sp_horse_category'] ) ? sanitize_text_field( wp_unslash( implode( ', ', $_POST['sp_horse_category'] ) ) ) : '';
		$sp_performance  = isset( $_POST['horse_perf'] ) ? sanitize_text_field( wp_unslash( implode( ', ', $_POST['horse_perf'] ) ) ) : '';
		$race_type       = isset( $_POST['race_type'] ) ? sanitize_text_field( wp_unslash( $_POST['race_type'] ) ) : '';
		$th_category     = isset( $_POST['th_horse_category'] ) ? sanitize_text_field( wp_unslash( $_POST['th_horse_category'] ) ) : '';
		$horse_name      = isset( $_POST['horse_name'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_name'] ) ) : '';
		$horse_sire      = isset( $_POST['horse_sire'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_sire'] ) ) : '';
		$horse_dam       = isset( $_POST['horse_dam'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_dam'] ) ) : '';
		$horse_year      = isset( $_POST['horse_year'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_year'] ) ) : '';
		$horse_colour    = isset( $_POST['horse_colour'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_colour'] ) ) : '';
		$horse_height    = isset( $_POST['horse_height'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_height'] ) ) : '';
		$horse_vices     = isset( $_POST['horse_vices'] ) ? sanitize_textarea_field( wp_unslash( $_POST['horse_vices'] ) ) : '';
		$horse_location  = isset( $_POST['horse_location'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_location'] ) ) : '';
		$horse_origin    = isset( $_POST['horse_origin'] ) ? sanitize_text_field( wp_unslash( $_POST['horse_origin'] ) ) : '';
		$horse_video     = isset( $_POST['video'] ) ? esc_url( $_POST['video'] ) : '';
		$horse_price     = isset( $_POST['horse_price'] ) ? absint( $_POST['horse_price'] ) : '';
		$reserve_price   = isset( $_POST['reserve_price'] ) ? absint( $_POST['reserve_price'] ) : '';
		$starting_bid    = isset( $_POST['starting_bid'] ) ? absint( $_POST['starting_bid'] ) : '';
		$display_reserve = isset( $_POST['display_reserve'] ) ? sanitize_text_field( wp_unslash( $_POST['display_reserve'] ) ) : '';
		$auc_duration    = isset( $_POST['auction_duration'] ) ? sanitize_text_field( wp_unslash( $_POST['auction_duration'] ) ) : '';
		$redirect_url    = '#';

		if ( 'Sport Horse Basic' === $submission_type ) {
			$redirect_url = $atts['url_pay_spb'];
		} elseif ( 'Sport Horse Enhanced' === $submission_type ) {
			$redirect_url = $atts['url_pay_spe'];
		} elseif ( 'Thoroughbred Listing' === $submission_type ) {
			$redirect_url = $atts['url_pay_thl'];
		} elseif ( 'Thoroughbred Auction' === $submission_type ) {
			$redirect_url = $atts['url_pay_tha'];
		}

		if ( ! empty( $horse_video ) ) {
			$description .= '<p><a class="video-link" href="' . $horse_video . '" data-lity><i class="fa fa-video-camera" aria-hidden="true"></i>Watch Video</a></p>';
		}

		$description .= '<ul>';
		$description .= ( ! empty( $horse_gender ) ) ? '<li>Gender: ' . $horse_gender . '</li>' : '';

		if ( 'Sport Horse Basic' === $submission_type || 'Sport Horse Enhanced' === $submission_type ) {
			$description .= ( ! empty( $sp_categories ) ) ? '<li>Category: ' . $sp_categories . '</li>' : '';
			$description .= ( ! empty( $sp_performance ) ) ? '<li>Performance: ' . $sp_performance . '</li>' : '';
		}

		if ( 'Thoroughbred Auction' === $submission_type || 'Thoroughbred Listing' === $submission_type ) {
			$description .= ( ! empty( $race_type ) ) ? '<li>Race Type: ' . $race_type . '</li>' : '';
			$description .= ( ! empty( $th_category ) ) ? '<li>Category: ' . $th_category . '</li>' : '';
		}

		if ( 'Thoroughbred Auction' === $submission_type ) {
			$description .= ( ! empty( $display_reserve ) && 'Yes' === $display_reserve ) ? '<li>Reserve Price: €' . $reserve_price . '</li>' : '';
			$description .= ( ! empty( $auc_duration ) ) ? '<li>Auction Duration: ' . $auc_duration . '</li>' : '';
		}

		$description .= ( ! empty( $horse_name ) ) ? '<li>Horse Name: ' . $horse_name . '</li>' : '';
		$description .= ( ! empty( $horse_sire ) ) ? '<li>Sire: ' . $horse_sire . '</li>' : '';
		$description .= ( ! empty( $horse_dam ) ) ? '<li>Dam: ' . $horse_dam . '</li>' : '';
		$description .= ( ! empty( $horse_year ) ) ? '<li>Year of Birth: ' . $horse_year . '</li>' : '';
		$description .= ( ! empty( $horse_colour ) ) ? '<li>Colour: ' . $horse_colour . '</li>' : '';
		$description .= ( ! empty( $horse_height ) ) ? '<li>Height: ' . $horse_height . '</li>' : '';
		$description .= ( ! empty( $horse_vices ) ) ? '<li>Vices: ' . $horse_vices . '</li>' : '';
		$description .= ( ! empty( $horse_origin ) ) ? '<li>Country of Origin: ' . $horse_origin . '</li>' : '';
		$description .= ( ! empty( $horse_location ) ) ? '<li>Current Location: ' . $horse_location . '</li>' : '';
		$seller       = $first_name . ' ' . $last_name;
		$description .= ( ! empty( $seller ) ) ? '<li>Seller: ' . $seller . '</li>' : '';
		$description .= '</ul>';

		if ( ! empty( $contact_no ) ) {
			$description .= '<p class="wsawl-link"><a href="tel:' . $contact_no . '" rel="noopener noreferrer"><i class="fa fa-phone mr-2"></i>Call Seller</a></p>';
		}

		if ( ! empty( $email ) ) {
			$description .= '<p class="wsawl-link"><a href="mailto:' . $email . '?subject=Contact via HorseSales.ie" rel="noopener noreferrer"><i class="fa fa-envelope-o mr-2"></i>Send Email</a></p>';
		}

		$my_post = array(
			'post_title'   => $listing_title,
			'post_type'    => 'product',
			'post_content' => $description,
			'post_excerpt' => $description,
		);

		/**
		 * Everything after wp_insert_post happens after the
		 * post is created (as the created id is required).
		 */
		$created_post_id = wp_insert_post( $my_post );

		if ( isset( $_POST['hs_featured_img_path'] ) && ! empty( $_POST['hs_featured_img_path'] ) ) {
			$featured_img_id = media_sideload_image( esc_url( $_POST['hs_featured_img_path'] ), $created_post_id, $listing_title, 'id' );
			update_post_meta( $created_post_id, '_thumbnail_id', $featured_img_id );
		}

		$extra_img_ids = array();

		if ( isset( $_POST['hs_img_2_path'] ) && ! empty( $_POST['hs_img_2_path'] ) ) {
			$img_2_id = media_sideload_image( esc_url( $_POST['hs_img_2_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_2_id );
		}

		if ( isset( $_POST['hs_img_3_path'] ) && ! empty( $_POST['hs_img_3_path'] ) ) {
			$img_3_id = media_sideload_image( esc_url( $_POST['hs_img_3_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_3_id );
		}

		if ( isset( $_POST['hs_img_4_path'] ) && ! empty( $_POST['hs_img_4_path'] ) ) {
			$img_4_id = media_sideload_image( esc_url( $_POST['hs_img_4_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_4_id );
		}

		if ( isset( $_POST['hs_img_5_path'] ) && ! empty( $_POST['hs_img_5_path'] ) ) {
			$img_5_id = media_sideload_image( esc_url( $_POST['hs_img_5_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_5_id );
		}

		if ( isset( $_POST['hs_pedigree_path'] ) && ! empty( $_POST['hs_pedigree_path'] ) ) {
			$pedigree_id = media_sideload_image( esc_url( $_POST['hs_pedigree_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $pedigree_id );
		}

		if ( isset( $_POST['hs_passport_path'] ) && ! empty( $_POST['hs_passport_path'] ) ) {
			$passport_id = media_sideload_image( esc_url( $_POST['hs_passport_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $passport_id );
		}

		if ( isset( $_POST['hs_vac_path'] ) && ! empty( $_POST['hs_vac_path'] ) ) {
			$vac_id = media_sideload_image( esc_url( $_POST['hs_vac_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $vac_id );
		}

		if ( isset( $_POST['hs_reprod_path'] ) && ! empty( $_POST['hs_reprod_path'] ) ) {
			$reprod_id = media_sideload_image( esc_url( $_POST['hs_reprod_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $reprod_id );
		}

		if ( isset( $_POST['hs_health_path'] ) && ! empty( $_POST['hs_health_path'] ) ) {
			$health_id = media_sideload_image( esc_url( $_POST['hs_health_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $health_id );
		}

		if ( ! empty( $extra_img_ids ) ) {
			$extra_img_ids = implode( ',', $extra_img_ids );
			update_post_meta( $created_post_id, '_product_image_gallery', $extra_img_ids );
		}

		if ( 'Sport Horse Basic' === $submission_type || 'Sport Horse Enhanced' === $submission_type ) {
			wp_set_object_terms( $created_post_id, 'sport-leisure-horses', 'product_cat' );
		}

		if ( 'Thoroughbred Listing' === $submission_type ) {
			wp_set_object_terms( $created_post_id, 'thoroughbred-listings', 'product_cat' );
		}

		if ( ! empty( $horse_price ) && ( 'Sport Horse Basic' === $submission_type || 'Sport Horse Enhanced' === $submission_type || 'Thoroughbred Listing' === $submission_type ) ) {
			update_post_meta( $created_post_id, '_price', $horse_price );
			update_post_meta( $created_post_id, '_regular_price', $horse_price );
		}

		if ( 'Thoroughbred Auction' === $submission_type ) {
			wp_set_object_terms( $created_post_id, 'thoroughbred-auctions', 'product_cat' );
			wp_set_object_terms( $created_post_id, 'auction', 'product_type' );
			update_post_meta( $created_post_id, '_auction_type', 'normal' );
			update_post_meta( $created_post_id, '_auction_reserved_price', $reserve_price );
			if ( ! empty( $starting_bid ) ) {
				update_post_meta( $created_post_id, '_auction_start_price', $starting_bid );
			}
		}

		hs_send_admin_notification( $seller, $contact_no, $email, $listing_title, $submission_type );

		?>

		<div class="alert alert-success" role="alert">Thank you for your submission!</div>
		<a class="btn btn-submit" href="<?php echo esc_url( $redirect_url ); ?>">Confirm & Pay</a>

		<?php
	}
}
