<?php
/**
 * The form submission handler for properties.
 *
 * @package HorsesalesSubmit
 */

require_once ABSPATH . 'wp-admin/includes/media.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/image.php';

/**
 * Register the shortcode.
 */
add_shortcode( 'prop_submission_handler', 'prop_submission_handler_callback' );

/**
 * The shortcode callback function.
 *
 * @param array $attributes The shortcode attributes.
 */
function prop_submission_handler_callback( $attributes ) {
	$atts = shortcode_atts(
		array(
			'url_pay_prop_listing' => site_url() . '/set-the-property-listing-payment-url',
		),
		$attributes,
		'prop_submission_handler'
	);

	$is_valid_nonce = ( isset( $_POST['add_property_form'] ) && wp_verify_nonce( $_POST['add_property_form'] ) ) ? true : false;

	if ( ! $is_valid_nonce ) {
		?>

		<div class="alert alert-danger" role="alert">Invalid form submission.</div>

		<?php

	} else {

		$submission    = isset( $_POST['submission_type'] ) ? sanitize_text_field( wp_unslash( $_POST['submission_type'] ) ) : '';
		$first_name    = isset( $_POST['first_name'] ) ? sanitize_text_field( wp_unslash( $_POST['first_name'] ) ) : '';
		$last_name     = isset( $_POST['last_name'] ) ? sanitize_text_field( wp_unslash( $_POST['last_name'] ) ) : '';
		$contact_no    = isset( $_POST['contact_no'] ) ? sanitize_text_field( wp_unslash( $_POST['contact_no'] ) ) : '';
		$email         = isset( $_POST['user_email'] ) ? sanitize_email( wp_unslash( $_POST['user_email'] ) ) : '';
		$listing_title = isset( $_POST['listing_title'] ) ? sanitize_text_field( wp_unslash( $_POST['listing_title'] ) ) : '';
		$description   = isset( $_POST['prop_desc'] ) ? sanitize_textarea_field( wp_unslash( $_POST['prop_desc'] ) ) : '';
		$prop_category = isset( $_POST['property_category'] ) ? sanitize_text_field( wp_unslash( $_POST['property_category'] ) ) : '';
		$option_agri   = isset( $_POST['option_agri'] ) ? sanitize_text_field( wp_unslash( $_POST['option_agri'] ) ) : '';
		$option_equine = isset( $_POST['option_equine'] ) ? sanitize_text_field( wp_unslash( $_POST['option_equine'] ) ) : '';
		$has_dwelling  = isset( $_POST['dwelling_house'] ) ? sanitize_text_field( wp_unslash( $_POST['dwelling_house'] ) ) : '';
		$num_acres     = isset( $_POST['acres'] ) ? absint( $_POST['acres'] ) : '';
		$prop_price    = isset( $_POST['property_price'] ) ? absint( $_POST['property_price'] ) : '';
		$location      = isset( $_POST['location'] ) ? sanitize_text_field( wp_unslash( $_POST['location'] ) ) : '';
		$google_maps   = isset( $_POST['google_maps'] ) ? esc_url( $_POST['google_maps'] ) : '';
		$prop_video    = isset( $_POST['video'] ) ? esc_url( $_POST['video'] ) : '';
		$payment_due   = isset( $_POST['payment_amount'] ) ? sanitize_text_field( wp_unslash( $_POST['payment_amount'] ) ) : '';
		$redirect_url  = $atts['url_pay_prop_listing'];
		$seller        = $first_name . ' ' . $last_name;

		if ( ! empty( $prop_video ) ) {
			$description .= '<p><a class="video-link" href="' . $prop_video . '" data-lity><i class="fa fa-video-camera" aria-hidden="true"></i>Watch Video</a></p>';
		}

		$description .= '<ul>';
		$description .= ( ! empty( $prop_category ) ) ? '<li>Category: ' . $prop_category . '</li>' : '';
		$description .= ( ! empty( $option_agri ) ) ? '<li>Property Type: ' . $option_agri . '</li>' : '';
		$description .= ( ! empty( $option_equine ) ) ? '<li>Property Type: ' . $option_equine . '</li>' : '';
		$description .= ( ! empty( $has_dwelling ) ) ? '<li>Contains Dwelling House: ' . $has_dwelling . '</li>' : '';
		$description .= ( ! empty( $num_acres ) ) ? '<li>No. of Acres: ' . $num_acres . '</li>' : '';
		$description .= ( ! empty( $location ) ) ? '<li>Site Location: ' . $location . '</li>' : '';
		$description .= ( ! empty( $seller ) ) ? '<li>Seller: ' . $seller . '</li>' : '';
		$description .= '</ul>';

		if ( ! empty( $google_maps ) ) {
			$description .= '<p class="wsawl-link"><a href="' . $google_maps . '" target="_blank" rel="noopener noreferrer"><i class="fa fa-map-marker mr-2"></i>Google Maps</a></p>';
		}

		if ( ! empty( $contact_no ) ) {
			$description .= '<p class="wsawl-link"><a href="tel:' . $contact_no . '" rel="noopener noreferrer"><i class="fa fa-phone mr-2"></i>Call Seller</a></p>';
		}

		if ( ! empty( $email ) ) {
			$description .= '<p class="wsawl-link"><a href="mailto:' . $email . '?subject=Contact via HorseSales.ie" rel="noopener noreferrer"><i class="fa fa-envelope-o mr-2"></i>Send Email</a></p>';
		}

		$my_post = array(
			'post_title'   => $listing_title,
			'post_type'    => 'product',
			'post_content' => $description,
			'post_excerpt' => $description,
		);

		/**
		 * Everything after wp_insert_post happens after the
		 * post is created (as the created id is required).
		 */
		$created_post_id = wp_insert_post( $my_post );

		if ( isset( $_POST['hs_featured_img_path'] ) && ! empty( $_POST['hs_featured_img_path'] ) ) {
			$featured_img_id = media_sideload_image( esc_url( $_POST['hs_featured_img_path'] ), $created_post_id, $listing_title, 'id' );
			update_post_meta( $created_post_id, '_thumbnail_id', $featured_img_id );
		}

		$extra_img_ids = array();

		if ( isset( $_POST['hs_img_2_path'] ) && ! empty( $_POST['hs_img_2_path'] ) ) {
			$img_2_id = media_sideload_image( esc_url( $_POST['hs_img_2_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_2_id );
		}

		if ( isset( $_POST['hs_img_3_path'] ) && ! empty( $_POST['hs_img_3_path'] ) ) {
			$img_3_id = media_sideload_image( esc_url( $_POST['hs_img_3_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_3_id );
		}

		if ( isset( $_POST['hs_img_4_path'] ) && ! empty( $_POST['hs_img_4_path'] ) ) {
			$img_4_id = media_sideload_image( esc_url( $_POST['hs_img_4_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_4_id );
		}

		if ( isset( $_POST['hs_img_5_path'] ) && ! empty( $_POST['hs_img_5_path'] ) ) {
			$img_5_id = media_sideload_image( esc_url( $_POST['hs_img_5_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_5_id );
		}

		if ( isset( $_POST['hs_img_6_path'] ) && ! empty( $_POST['hs_img_6_path'] ) ) {
			$img_6_id = media_sideload_image( esc_url( $_POST['hs_img_6_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_6_id );
		}

		if ( isset( $_POST['hs_img_7_path'] ) && ! empty( $_POST['hs_img_7_path'] ) ) {
			$img_7_id = media_sideload_image( esc_url( $_POST['hs_img_7_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_7_id );
		}

		if ( isset( $_POST['hs_img_8_path'] ) && ! empty( $_POST['hs_img_8_path'] ) ) {
			$img_8_id = media_sideload_image( esc_url( $_POST['hs_img_8_path'] ), $created_post_id, $listing_title, 'id' );
			array_push( $extra_img_ids, $img_8_id );
		}

		if ( ! empty( $extra_img_ids ) ) {
			$extra_img_ids = implode( ',', $extra_img_ids );
			update_post_meta( $created_post_id, '_product_image_gallery', $extra_img_ids );
		}

		if ( ! empty( $prop_price ) ) {
			update_post_meta( $created_post_id, '_price', $prop_price );
			update_post_meta( $created_post_id, '_regular_price', $prop_price );
		}

		wp_set_object_terms( $created_post_id, 'farms-estates', 'product_cat' );

		hs_send_admin_notification( $seller, $contact_no, $email, $listing_title, $submission );

		?>

		<div class="alert alert-success" role="alert">Thank you for your submission!</div>
		<a class="btn btn-submit" href="<?php echo esc_url( $redirect_url ); ?>">Confirm & Pay</a>

		<?php
	}

}
