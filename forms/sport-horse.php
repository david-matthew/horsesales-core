<?php
/**
 * The form allowing users to submit a sport horse.
 *
 * @package HorsesalesSubmit
 */

/**
 * Register the shortcode.
 */
add_shortcode( 'hs_sport_horse_form', 'hs_sport_horse_form_callback' );

/**
 * The shortcode callback function.
 *
 * @param array $attributes The shortcode attributes.
 */
function hs_sport_horse_form_callback( $attributes ) {
	global $hs_should_add_script;
	$hs_should_add_script = true;

	$atts = shortcode_atts(
		array(
			'url_submission' => site_url() . '/set-the-form-submission-handler-url',
			'url_tscs'       => site_url() . '/set-the-tscs-url',
		),
		$attributes,
		'hs_sport_horse_form'
	);

	?>

	<div class="alert alert-info alert-dismissible fade show" role="alert">

		<span>Note that fields marked with an asterisk <span class="required">*</span> are required.</span>

		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

	</div>

	<form id="frontend-submit" action="<?php echo esc_url( $atts['url_submission'] ); ?>" method="post">

		<?php wp_nonce_field( -1, 'add_horse_form' ); ?>

		<h3	data-toggle="collapse" data-target="#user-details"><i class="fa fa-user-circle" aria-hidden="true"></i>Your Details</h3>
		<hr>

		<div id="user-details" class="collapse show form-section" aria-expanded="true">

			<div class="form-row">

				<div class="col-6 form-group">

					<label for="first-name">First Name<span class="required">*</span></label>

					<input required type="text" id="first-name" name="first_name" class="form-control">

				</div>

				<div class="col-6 form-group">

					<label for="last-name">Last Name<span class="required">*</span></label>

					<input required type="text" id="last-name" name="last_name" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="contact-no">Contact Number<span class="required">*</span></label>

					<input required type="tel" id="contact-no" name="contact_no" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="user-email">Email Address<span class="required">*</span></label>

					<input required type="email" id="user-email" name="user_email" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

				<a class="btn btn-next" data-toggle="collapse" href="#horse-details">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>			

		</div>

		<h3 data-toggle="collapse" data-target="#horse-details"><i class="fa fa-list-alt" aria-hidden="true"></i>Horse Details</h3>
		<hr>

		<div id="horse-details" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="listing-title">Listing Title<span class="required">*</span></label>

					<input required type="text" id="listing-title" name="listing_title" class="form-control" placeholder="Try to keep this to four/five words, or less if possible.">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="horse-desc">Description<span class="required">*</span></label>

					<textarea required id="horse-desc" name="horse_desc" cols="40" rows="6" class="form-control" placeholder="Enter a description of your horse here. Stick to the highlights, and think of points that would attract potential buyers."></textarea>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="horse-vices">Vices</label>

					<textarea id="horse-vices" name="horse_vices" cols="40" rows="2" class="form-control" placeholder="List the vices here (if any)."></textarea>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-price">Price (€)</label>

					<input type="number" id="horse-price" name="horse_price" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-name">Horse Name</label>

					<input type="text" id="horse-name" name="horse_name" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-sire">Sire</label>

					<input type="text" id="horse-sire" name="horse_sire" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-dam">Dam</label>

					<input type="text" id="horse-dam" name="horse_dam" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-year">Year of Birth</label>

					<input type="text" id="horse-year" name="horse_year" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-colour">Colour</label>

					<input type="text" id="horse-colour" name="horse_colour" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-height">Height</label>

					<input type="text" id="horse-height" name="horse_height" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-gender">Gender<span class="required">*</span></label>

					<select required name="horse_gender" id="horse-gender" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<option value="Colt">Colt</option>
						<option value="Filly">Filly</option>
						<option value="Gelding">Gelding</option>
						<option value="Mare">Mare</option>
						<option value="Stallion">Stallion</option>
					</select>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label>Category (tick all that apply)</label><br/>

					<input type="checkbox" name="sp_horse_category[]" value="Allrounder"> Allrounder<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Cob"> Cob<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Draught"> Draught<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Dressage"> Dressage<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Eventer"> Eventer<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Endurance"> Endurance<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Hunter"> Hunter<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Hunter/Jumper"> Hunter/Jumper<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Pony"> Pony<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Showjumper"> Showjumper<br/>
					<input type="checkbox" name="sp_horse_category[]" value="Young Stock"> Young Stock<br/>

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label>Performance (tick all that apply)</label><br/>

					<input type="checkbox" name="horse_perf[]" value="Unbroken"> Unbroken<br/>
					<input type="checkbox" name="horse_perf[]" value="Hunter-trialled"> Hunter-trialled<br/>
					<input type="checkbox" name="horse_perf[]" value="Evented"> Evented<br/>
					<input type="checkbox" name="horse_perf[]" value="Competed at Training Shows"> Competed at Training Shows<br/>
					<input type="checkbox" name="horse_perf[]" value="Competed in SJI or FEI"> Competed in SJI or FEI<br/>
					<input type="checkbox" name="horse_perf[]" value="Pony Clubbed"> Pony Clubbed<br/>
					<input type="checkbox" name="horse_perf[]" value="Lunging Over Poles"> Lunging Over Poles<br/>

				</div>		

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-location">Current Location<span class="required">*</span></label>

					<input required type="text" id="horse-location" name="horse_location" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="horse-origin">Country of Origin</label>

					<input type="text" id="horse-origin" name="horse_origin" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#media">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#media"><i class="fa fa-picture-o" aria-hidden="true"></i>Images / Video</h3><hr>

		<div id="media" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Featured image<span class="required">*</span></label><br>

					<div id="hs-featured-img-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-featured-img-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input required type="file" name="hs_featured_img" id="hs-featured-img" value="" accept="image/*" onchange="hsImageUpload('hs-featured-img'); return false;">
					<input type="text" name="hs_featured_img_path" id="hs-featured-img-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 2</label><br>

					<div id="hs-img-2-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-2-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_2" id="hs-img-2" value="" accept="image/*" onchange="hsImageUpload('hs-img-2'); return false;"/>
					<input type="text" name="hs_img_2_path" id="hs-img-2-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 3</label><br>

					<div id="hs-img-3-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-3-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_3" id="hs-img-3" value="" accept="image/*" onchange="hsImageUpload('hs-img-3'); return false;"/>
					<input type="text" name="hs_img_3_path" id="hs-img-3-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 4</label><br>

					<div id="hs-img-4-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-4-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_4" id="hs-img-4" value="" accept="image/*" onchange="hsImageUpload('hs-img-4'); return false;"/>
					<input type="text" name="hs_img_4_path" id="hs-img-4-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 5</label><br>

					<div id="hs-img-5-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-5-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_5" id="hs-img-5" value="" accept="image/*" onchange="hsImageUpload('hs-img-5'); return false;"/>
					<input type="text" name="hs_img_5_path" id="hs-img-5-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="video">YouTube/Vimeo URL</label>

					<input type="url" id="video" name="video" class="form-control" placeholder="A valid URL must start with http or https">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#confirmation">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#confirmation"><i class="fa fa-check-square-o" aria-hidden="true"></i>Confirmation</h3><hr>

		<div id="confirmation" class="collapse form-section">

			<div class="form-row">

				<label>Select your package<span class="required">*</span></label>

			</div>

			<div class="form-row">

				<div id="basic-package" class="col-6 package" onclick="selectPackage('basic-package')">

					<div class="alert alert-primary" role="alert">€15 Package (VAT Inclusive)</div>
					<ul>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Listed on the horsesales.ie platform</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Quality Advert to best promote your horse</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Optimised Photo and Video Content</li>
						<li><i class="fa fa-times-circle" aria-hidden="true"></i>Promoted and marketed across all our social media channels</li>
						<li><i class="fa fa-times-circle" aria-hidden="true"></i>Marketed to affiliate partners in the US and Europe</li>
						<li><i class="fa fa-times-circle" aria-hidden="true"></i>Promoted once on our Weekly Featured News Video</li>
					</ul>

				</div>

				<div id="enhanced-package" class="col-6 package" onclick="selectPackage('enhanced-package')">

					<div class="alert alert-success" role="alert">€25 Package (VAT Inclusive)</div>
					<ul>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Listed on the horsesales.ie platform</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Quality Advert to best promote your horse</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Optimised Photo and Video Content</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Promoted and marketed across all our social media channels</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Marketed to affiliate partners in the US and Europe</li>
						<li><i class="fa fa-check-circle" aria-hidden="true"></i>Promoted once on our Weekly Featured News Video</li>
					</ul>

				</div>

				<input type="text" id="submission-type" name="submission_type" value="" hidden required />

			</div>

			<div class="form-row">

				<div class="col-12">

					<label>Terms and Conditions<span class="required">*</span></label><br>

					<input required type="checkbox" id="tcs" name="tcs" value="yes" /> By ticking this box you accept our <a href="<?php echo esc_url( $atts['url_tscs'] ); ?>" target="_blank">Terms and Conditions</a>.<br/>

					<button class="btn btn-submit mt-3" type="submit" onclick="hsShowLoader()">Submit & Pay</button>

					<div id="form-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>

				</div>

			</div>

		</div>

	</form>

	<?php
}
