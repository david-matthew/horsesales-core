<?php
/**
 * The form allowing users to submit a property.
 *
 * @package HorsesalesSubmit
 */

/**
 * Register the shortcode.
 */
add_shortcode( 'hs_property_form', 'hs_property_form_callback' );

/**
 * The shortcode callback function.
 *
 * @param array $attributes The shortcode attributes.
 */
function hs_property_form_callback( $attributes ) {
	global $hs_should_add_script;
	$hs_should_add_script = true;

	$atts = shortcode_atts(
		array(
			'url_submission' => site_url() . '/set-the-form-submission-handler-url',
			'url_tscs'       => site_url() . '/set-the-tscs-url',
		),
		$attributes,
		'hs_property_form'
	);

	?>

	<div class="alert alert-info alert-dismissible fade show" role="alert">

		<span>Note that fields marked with an asterisk <span class="required">*</span> are required.</span>

		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

	</div>

	<form id="frontend-submit" action="<?php echo esc_url( $atts['url_submission'] ); ?>" method="post">

		<?php wp_nonce_field( -1, 'add_property_form' ); ?>

		<h3	data-toggle="collapse" data-target="#user-details"><i class="fa fa-user-circle" aria-hidden="true"></i>Your Details</h3>
		<hr>

		<div id="user-details" class="collapse show form-section" aria-expanded="true">

			<div class="form-row">

				<div class="col-6 form-group">

					<label for="first-name">First Name<span class="required">*</span></label>

					<input required type="text" id="first-name" name="first_name" class="form-control">

				</div>

				<div class="col-6 form-group">

					<label for="last-name">Last Name<span class="required">*</span></label>

					<input required type="text" id="last-name" name="last_name" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="contact-no">Contact Number<span class="required">*</span></label>

					<input required type="tel" id="contact-no" name="contact_no" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="user-email">Email Address<span class="required">*</span></label>

					<input required type="email" id="user-email" name="user_email" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

				<a class="btn btn-next" data-toggle="collapse" href="#property-details">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>			

		</div>

		<h3 data-toggle="collapse" data-target="#property-details"><i class="fa fa-home" aria-hidden="true"></i>Property Details</h3>
		<hr>

		<div id="property-details" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="property-category">Property Category<span class="required">*</span></label>

					<select required name="property_category" id="property-category" class="form-control" onchange="hsShowPropType()">
						<option value="" disabled selected>Please Select</option>
						<option value="Agricultural Land">Agricultural Land</option>
						<option value="Equine Stud">Equine Stud</option>
					</select>

				</div>

			</div>

			<div class="form-row">

				<div id="type-agri" class="col-12 form-group">

					<label for="option-agri">Property Type</label>

					<select name="option_agri" id="option-agri" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<option value="Tillage">Tillage</option>
						<option value="Dairy">Dairy</option>
						<option value="Mixed">Mixed</option>
					</select>

				</div>

			</div>

			<div class="form-row">

				<div id="type-equine" class="col-12 form-group">

					<label for="option-equine">Property Type</label>

					<select name="option_equine" id="option-equine" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<option value="Tillage">Flat</option>
						<option value="Dairy">Hurdles</option>
						<option value="Mixed (Farm & Equine)">Mixed (Farm & Equine)</option>
					</select>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="dwelling-house">Contains Dwelling House?<span class="required">*</span></label>

					<select required name="dwelling_house" id="dwelling-house" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<option value="Yes">Yes</option>
						<option value="No">No</option>
					</select>

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="acres">No. of Acres</label>

					<input type="number" id="acres" name="acres" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="location">Site Location<span class="required">*</span></label>

					<input required type="text" id="location" name="location" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="google-maps">Google Maps URL</label>

					<input type="url" name="google_maps" id="google-maps" class="form-control" placeholder="A valid URL must start with http or https">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="property-price">Price (€)</label>

					<input type="number" id="property-price" name="property_price" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="listing-title">Listing Title<span class="required">*</span></label>

					<input required type="text" id="listing-title" name="listing_title" class="form-control" placeholder="Try to keep this to four/five words, or less if possible.">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="prop-desc">Description<span class="required">*</span></label>

					<textarea required id="prop-desc" name="prop_desc" cols="40" rows="6" class="form-control" placeholder="Enter a description of your property here. Stick to the highlights, and think of points that would attract potential buyers."></textarea>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#media">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#media"><i class="fa fa-picture-o" aria-hidden="true"></i>Images / Video</h3><hr>

		<div id="media" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Featured image<span class="required">*</span></label><br>

					<div id="hs-featured-img-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-featured-img-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input required type="file" name="hs_featured_img" id="hs-featured-img" value="" accept="image/*" onchange="hsImageUpload('hs-featured-img'); return false;">
					<input type="text" name="hs_featured_img_path" id="hs-featured-img-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 2</label><br>

					<div id="hs-img-2-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-2-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_2" id="hs-img-2" value="" accept="image/*" onchange="hsImageUpload('hs-img-2'); return false;"/>
					<input type="text" name="hs_img_2_path" id="hs-img-2-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 3</label><br>

					<div id="hs-img-3-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-3-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_3" id="hs-img-3" value="" accept="image/*" onchange="hsImageUpload('hs-img-3'); return false;"/>
					<input type="text" name="hs_img_3_path" id="hs-img-3-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 4</label><br>

					<div id="hs-img-4-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-4-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_4" id="hs-img-4" value="" accept="image/*" onchange="hsImageUpload('hs-img-4'); return false;"/>
					<input type="text" name="hs_img_4_path" id="hs-img-4-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 5</label><br>

					<div id="hs-img-5-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-5-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_5" id="hs-img-5" value="" accept="image/*" onchange="hsImageUpload('hs-img-5'); return false;"/>
					<input type="text" name="hs_img_5_path" id="hs-img-5-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 6</label><br>

					<div id="hs-img-6-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-6-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_6" id="hs-img-6" value="" accept="image/*" onchange="hsImageUpload('hs-img-6'); return false;"/>
					<input type="text" name="hs_img_6_path" id="hs-img-6-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 7</label><br>

					<div id="hs-img-7-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-7-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_7" id="hs-img-7" value="" accept="image/*" onchange="hsImageUpload('hs-img-7'); return false;"/>
					<input type="text" name="hs_img_7_path" id="hs-img-7-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 8</label><br>

					<div id="hs-img-8-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="hs-img-8-preview" class="img-additional img-thumbnail mb-3" src=""/>
					<input type="file" name="hs_img_8" id="hs-img-8" value="" accept="image/*" onchange="hsImageUpload('hs-img-8'); return false;"/>
					<input type="text" name="hs_img_8_path" id="hs-img-8-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="video">YouTube/Vimeo URL</label>

					<input type="url" id="video" name="video" class="form-control" placeholder="A valid URL must start with http or https">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#confirmation">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#confirmation"><i class="fa fa-check-square-o" aria-hidden="true"></i>Confirmation</h3><hr>

		<div id="confirmation" class="collapse form-section">

			<div class="form-row">

				<div class="col-12">

					<input type="text" id="submission-type" name="submission_type" value="Property Listing" hidden required />

					<label>Terms and Conditions<span class="required">*</span></label><br>

					<input required type="checkbox" id="tcs" name="tcs" value="yes" /> By ticking this box you accept our <a href="<?php echo esc_url( $atts['url_tscs'] ); ?>" target="_blank">Terms and Conditions</a>.<br/>

					<button class="btn btn-submit mt-3" type="submit" onclick="hsShowLoader()">Submit & Pay</button>

					<div id="form-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>

				</div>

			</div>

		</div>

	</form>

	<?php
}

