<?php
/**
 * The form allowing users to submit a trade/service to the marketplace.
 *
 * @package HorsesalesSubmit
 */

/**
 * Register the shortcode.
 */
add_shortcode( 'hs_marketplace_form', 'hs_marketplace_form_callback' );

/**
 * The shortcode callback function.
 *
 * @param array $attributes The shortcode attributes.
 */
function hs_marketplace_form_callback( $attributes ) {
	global $hs_should_add_script;
	$hs_should_add_script = true;

	$atts = shortcode_atts(
		array(
			'url_submission' => site_url() . '/set-the-form-submission-handler-url',
			'url_tscs'       => site_url() . '/set-the-tscs-url',
		),
		$attributes,
		'hs_property_form'
	);

	?>

	<div class="alert alert-info alert-dismissible fade show" role="alert">

		<span>Note that fields marked with an asterisk <span class="required">*</span> are required.</span>

		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

	</div>

	<form id="frontend-submit" action="<?php echo esc_url( $atts['url_submission'] ); ?>" method="post">

		<?php wp_nonce_field( -1, 'marketplace_form' ); ?>

		<h3	data-toggle="collapse" data-target="#user-details"><i class="fa fa-user-circle" aria-hidden="true"></i>Your Details</h3>
		<hr>

		<div id="user-details" class="collapse show form-section" aria-expanded="true">

			<div class="form-row">

				<div class="col-6 form-group">

					<label for="first-name">First Name<span class="required">*</span></label>

					<input required type="text" id="first-name" name="first_name" class="form-control">

				</div>

				<div class="col-6 form-group">

					<label for="last-name">Last Name<span class="required">*</span></label>

					<input required type="text" id="last-name" name="last_name" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="contact-no">Contact Number<span class="required">*</span></label>

					<input required type="tel" id="contact-no" name="contact_no" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="user-email">Email Address<span class="required">*</span></label>

					<input required type="email" id="user-email" name="user_email" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

				<a class="btn btn-next" data-toggle="collapse" href="#business-details">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>			

		</div>

		<h3 data-toggle="collapse" data-target="#business-details"><i class="fa fa-building-o" aria-hidden="true"></i>Business Details</h3>
		<hr>

		<div id="business-details" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="business-name">Business Name<span class="required">*</span></label>

					<input required type="text" id="business-name" name="business_name" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="business-email">Email (if different from above)</label>

					<input type="email" id="business-email" name="business_email" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="business-contact">Contact No. (if different from above)</label>

					<input type="text" id="business-contact" name="business_contact" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="street-address">Street Address</label>

					<input type="text" id="street-address" name="street_address" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="county">County<span class="required">*</span></label>

					<select required name="county" id="county" class="form-control">
						<option value="" disabled selected>Please Select</option>
						<?php

						$service_locations = get_categories(
							array(
								'taxonomy'   => 'service-location',
								'orderby'    => 'name',
								'show_count' => 1,
								'pad_counts' => 0,
								'title_li'   => '',
								'hide_empty' => false,
							)
						);

						// Loop through them and display as option elements.
						foreach ( $service_locations as $loc ) {
							echo '<option value="' . esc_html( $loc->slug ) . '">' . esc_html( $loc->name ) . '</option>';
						}

						?>
					</select>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<div class="alert alert-info alert-dismissible fade show" role="alert">

						<span>If your business has more than one address, include the <strong>primary address above</strong> and mention the additional locations in the description below.</span>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					</div>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="county">Category (tick all that apply)</label><br/>

					<?php

					$service_categories = get_categories(
						array(
							'taxonomy'   => 'service-category',
							'orderby'    => 'name',
							'show_count' => 1,
							'pad_counts' => 0,
							'title_li'   => '',
							'hide_empty' => false,
						)
					);

					// Loop through them and display as option elements.
					foreach ( $service_categories as $sc ) {
						echo '<input type="checkbox" name="service_category[]" value="' . esc_html( $sc->slug ) . '"> ' . esc_html( $sc->name ) . '<br>';
					}

					?>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="service-desc">Description<span class="required">*</span></label>

					<textarea required id="service-desc" name="service_desc" cols="40" rows="6" class="form-control" placeholder="Enter a description of your business here. We recommend at least 100 words (i.e. a couple of short paragraphs) or more if you prefer. The more detailed, the better."></textarea>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#service-links">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#service-links"><i class="fa fa-link" aria-hidden="true"></i>Links / Social Media</h3>
		<hr>

		<div id="service-links" class="collapse form-section">

			<div class="form-row">

				<div class="col-12">

					<div class="alert alert-info alert-dismissible fade show" role="alert">

						<span>Please note that a valid URL must start with <strong>http</strong> or <strong>https</strong></span>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					</div>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="website">Website</label>

					<input type="url" id="website" name="website" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="google-maps">Google Maps</label>

					<input type="url" id="google-maps" name="google_maps" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="facebook">Facebook</label>

					<input type="url" id="facebook" name="facebook" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="instagram">Instagram</label>

					<input type="url" id="instagram" name="instagram" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 col-sm-6 form-group">

					<label for="twitter">Twitter</label>

					<input type="url" id="twitter" name="twitter" class="form-control">

				</div>

				<div class="col-12 col-sm-6 form-group">

					<label for="linkedin">LinkedIn</label>

					<input type="url" id="linkedin" name="linkedin" class="form-control">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#media">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#media"><i class="fa fa-picture-o" aria-hidden="true"></i>Images / Video</h3><hr>

		<div id="media" class="collapse form-section">

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Featured image<span class="required">*</span></label><br>

					<div id="feat-img-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="feat-img-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input required type="file" name="feat_img" id="feat-img" value="" accept="image/*" onchange="hsImageUpload('feat-img'); return false;">
					<input type="text" name="feat_img_path" id="feat-img-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Logo (works best square-cropped)</label><br>

					<div id="business-logo-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="business-logo-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input type="file" name="business_logo" id="business-logo" value="" accept="image/*" onchange="hsImageUpload('business-logo'); return false;">
					<input type="text" name="business_logo_path" id="business-logo-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 3</label><br>

					<div id="img-3-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="img-3-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input type="file" name="img_3" id="img-3" value="" accept="image/*" onchange="hsImageUpload('img-3'); return false;">
					<input type="text" name="img_3_path" id="img-3-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 4</label><br>

					<div id="img-4-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="img-4-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input type="file" name="img_4" id="img-4" value="" accept="image/*" onchange="hsImageUpload('img-4'); return false;">
					<input type="text" name="img_4_path" id="img-4-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label>Image 5</label><br>

					<div id="img-5-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
					<img id="img-5-preview" class="img-fluid img-thumbnail mb-3" src=""/>
					<input type="file" name="img_5" id="img-5" value="" accept="image/*" onchange="hsImageUpload('img-5'); return false;">
					<input type="text" name="img_5_path" id="img-5-path" value="" hidden/>

				</div>

			</div>

			<div class="form-row">

				<div class="col-12 form-group">

					<label for="video">YouTube URL</label>

					<input type="url" id="video" name="video" class="form-control" placeholder="A valid URL must start with http or https">

				</div>

			</div>

			<div class="form-row">

				<div class="col-12">

					<a class="btn btn-next" data-toggle="collapse" href="#confirmation">Next<i class="fa fa-caret-down ml-1" aria-hidden="true"></i></a>

				</div>

			</div>

		</div>

		<h3 data-toggle="collapse" data-target="#confirmation"><i class="fa fa-check-square-o" aria-hidden="true"></i>Confirmation</h3><hr>

		<div id="confirmation" class="collapse form-section">

			<div class="form-row">

				<div class="col-12">

					<input type="text" id="submission-type" name="submission_type" value="Marketplace Listing" hidden required />

					<label>Terms and Conditions<span class="required">*</span></label><br>

					<input required type="checkbox" id="tcs" name="tcs" value="yes" /> By ticking this box you accept our <a href="<?php echo esc_url( $atts['url_tscs'] ); ?>" target="_blank">Terms and Conditions</a>.<br/>

					<button class="btn btn-submit mt-3" type="submit" onclick="hsShowLoader()">Submit & Pay</button>

					<div id="form-spinner" class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>

				</div>

			</div>

		</div>

	</form>

	<?php
}
