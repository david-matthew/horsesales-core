<?php
/**
 * The file handling email sending.
 *
 * @package HorsesalesSubmit
 */

/**
 * The function to send admin email notifications.
 *
 * @param string $seller The seller's name.
 * @param string $contact_no The seller's contact number.
 * @param string $email The seller's email address.
 * @param string $title The title of the submitted listing.
 * @param string $type The type of listing.
 */
function hs_send_admin_notification( $seller, $contact_no, $email, $title, $type ) {
	$to      = ! get_option( 'admin_email' ) ? 'service@horsesales.ie' : get_option( 'admin_email' );
	$subject = 'New Submission via ' . get_site_url();
	$body    = '<p>' . $seller . ' just sent a submission via ' . get_site_url() . '</p>';
	$body   .= '<ul><li>Purchase: ' . $type . '</li>';
	$body   .= '<li>Contact No: ' . $contact_no . '</li>';
	$body   .= '<li>Email: ' . $email . '</li>';
	$body   .= '<li>Listing Title: ' . $title . '</li></ul>';
	$headers = array( 'Content-Type: text/html; charset=UTF-8' );
	wp_mail( $to, $subject, $body, $headers );
}
