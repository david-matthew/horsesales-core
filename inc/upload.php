<?php
/**
 * The file handling image uploading and compression.
 *
 * @package HorsesalesSubmit
 */

/**
 * Localize the image uploading script.
 */
function hs_localize_script() {
	wp_localize_script( 'img-uploader', 'imgUploader', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'hs_localize_script' );

/**
 * Handle the server-side image uploading.
 */
function hs_img_upload() {
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once ABSPATH . 'wp-admin/includes/file.php';
	}
	add_action( 'wp_handle_upload', 'hs_compress_img' );
	$overrides     = array( 'test_form' => false );
	$uploaded_file = $_POST['file'];
	$posted_data   = isset( $_POST ) ? $_POST : array();
	$file_data     = isset( $_FILES ) ? $_FILES : array();
	$data          = array_merge( $posted_data, $file_data );
	$response      = array();
	$move_file     = wp_handle_upload( $data['file'], $overrides );
	remove_action( 'wp_handle_upload', 'hs_compress_img' );
	echo json_encode( $move_file );
	wp_die();
}
add_action( 'wp_ajax_hs_img_upload', 'hs_img_upload' );
add_action( 'wp_ajax_nopriv_hs_img_uploadd', 'hs_img_upload' );

/**
 * Compress the image.
 *
 * @param data $data The image data.
 */
function hs_compress_img( $data ) {
	$max_width  = 1280;
	$max_height = 1280;
	$quality    = 80;
	if ( $data['type'] !== 'image/jpeg' && $data['type'] !== 'image/jpg' ) {
		return $data;
	}
	$image_editor = wp_get_image_editor( $data['file'] );
	if ( is_wp_error( $image_editor ) ) {
		return wp_die( $image_editor );
	}
	$sizes = $image_editor->get_size();
	if ( $sizes['width'] > $max_width || $sizes['height'] > $max_height ) {
		$image_editor->resize( $max_width, $max_height, false );
	}
	$image_editor->set_quality( $quality );
	$image_editor->save( $data['file'] );
	return $data;
}
